/**
 * 
 */
package trial;

import com.example.trial.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

/**
 * @author George
 * 
 */
public class trial extends Activity {

	static int screenHeight;
	static int iconWidth;
	private int screenWidth;
	private GridView boardGridView;
	int playerTurnColor;
	int homePlayerColor;
	int agentPlayerColor;
	int[][] board;
	static Point[] winningLocs;
	static int lastPosition;
	Button reset;
	private int bounceTill;
	private boolean notWait;
	private GridView frontGridView;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Display display = getWindowManager().getDefaultDisplay();
		try {

			screenHeight = display.getHeight();
			screenWidth = display.getWidth();
		} catch (NoSuchMethodError e) {
			// API lower than 12, revert to deprecated methods
			DisplayMetrics metrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);
			screenHeight = metrics.heightPixels;
			screenWidth = metrics.widthPixels;
		}
		notWait = true;
		iconWidth = (screenWidth / 7);
		setContentView(R.layout.trial);
		board = new int[6][7];
		homePlayerColor = Color.GREEN;
		agentPlayerColor = Color.RED;
		playerTurnColor = homePlayerColor;
		reset = (Button) findViewById(R.id.reset);
		reset.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				onCreate(new Bundle());
			}

		});
		boardGridView = (GridView) findViewById(R.id.gridview);
		boardGridView.setAdapter( new BoardAdapter(this,1));
		frontGridView = (GridView) findViewById(R.id.gridView1);
		frontGridView.setAdapter(new BoardAdapter(this, 0));
		frontGridView.setOnTouchListener(new OnTouchListener() {
			private int boardPositionToChange;
			private int frontGridViewPosition;

			private void coloring(View v, MotionEvent event) {
				int x = (int) (event.getXPrecision() * event.getX());
				if (x < screenWidth / 7) {
					lastPosition = 0;
				} else if (x < ((screenWidth * 2) / 7)) {
					lastPosition = 1;
				} else if (x < ((screenWidth * 3) / 7)) {
					lastPosition = 2;
				} else if (x < ((screenWidth * 4) / 7)) {
					lastPosition = 3;
				} else if (x < ((screenWidth * 5) / 7)) {
					lastPosition = 4;
				} else if (x < ((screenWidth * 6) / 7)) {
					lastPosition = 5;
				} else {
					lastPosition = 6;
				}
				((BoardAdapter)boardGridView.getAdapter()).board[lastPosition].color = playerTurnColor;
				((BoardAdapter)boardGridView.getAdapter()).board[lastPosition].refreshDrawableState();
				colorCustom(lastPosition, Color.GRAY);
			}

			private void colorCustom(int lastPosition, int color) {
				int i = lastPosition + 7;
				while (i < ((BoardAdapter)boardGridView.getAdapter()).board.length) {
					if (((BoardAdapter)boardGridView.getAdapter()).board[i].color != homePlayerColor
							&& ((BoardAdapter)boardGridView.getAdapter()).board[i].color != agentPlayerColor) {
						((BoardAdapter)boardGridView.getAdapter()).board[i].color = color;
						((BoardAdapter)boardGridView.getAdapter()).board[i].refreshDrawableState();
						}
					i = i + 7;
				}
			}

			private void clearing() {
				((BoardAdapter)boardGridView.getAdapter()).board[lastPosition].color =Color.TRANSPARENT;
				((BoardAdapter)boardGridView.getAdapter()).board[lastPosition].refreshDrawableState();
				colorCustom(lastPosition, Color.WHITE);
			}
			@Override
			public boolean onTouch(final View v, final MotionEvent event) {
				((BoardAdapter)boardGridView.getAdapter()).setAble(false);
				((BoardAdapter)frontGridView.getAdapter()).setAble(false);
				if(notWait) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					int x = (int) (event.getXPrecision() * event.getX());
					boardPositionToChange = -1;
					frontGridViewPosition = 0;
					if (x < screenWidth / 7) {
						boardPositionToChange = play(frontGridViewPosition);
					} else if (x < ((screenWidth * 2) / 7)) {
						frontGridViewPosition++;
						boardPositionToChange = play(frontGridViewPosition);
					} else if (x < ((screenWidth * 3) / 7)) {
						frontGridViewPosition = frontGridViewPosition + 2;
						boardPositionToChange = play(frontGridViewPosition);
					} else if (x < ((screenWidth * 4) / 7)) {
						frontGridViewPosition = frontGridViewPosition + 3;
						boardPositionToChange = play(frontGridViewPosition);
					} else if (x < ((screenWidth * 5) / 7)) {
						frontGridViewPosition = frontGridViewPosition + 4;
						boardPositionToChange = play(frontGridViewPosition);
					} else if (x < ((screenWidth * 6) / 7)) {
						frontGridViewPosition = frontGridViewPosition + 5;
						boardPositionToChange = play(frontGridViewPosition);
					} else {
						frontGridViewPosition = frontGridViewPosition + 6;
						boardPositionToChange = play(frontGridViewPosition);
					}
					if (boardPositionToChange != -1) {
								 TranslateAnimation animator= new TranslateAnimation(0f, 0F, 0f, ((iconWidth*bounceTill)));
								 animator.setStartOffset(200);
								 animator.setDuration(500);
								 animator.setInterpolator(new BounceInterpolator());
						         animator.setAnimationListener(new AnimationListener() {
								
									@Override
									public void onAnimationStart(Animation animation) {
										// TODO Auto-generated method stub
										clearing();
										notWait=false;
												((BoardAdapter)frontGridView.getAdapter()).board[frontGridViewPosition].color = playerTurnColor;
										}
									
									@Override
									public void onAnimationRepeat(Animation animation) {
										// TODO Auto-generated method stub
										
									}
									
									@Override
									public void onAnimationEnd(Animation animation) {
												((BoardAdapter)frontGridView.getAdapter()).board[frontGridViewPosition].color = Color.TRANSPARENT;
												((BoardAdapter)boardGridView.getAdapter()).board[boardPositionToChange].color = playerTurnColor;
												if (isWinner(boardPositionToChange, frontGridViewPosition)) {
													Toast.makeText(trial.this, "winning move!",
															Toast.LENGTH_SHORT).show();
													if(winningLocs != null) {
														for(int i=0;i<winningLocs.length;i++) {
														((BoardAdapter)boardGridView.getAdapter()).board[winningLocs[i].getX()+7+(winningLocs[i].getY()*7)].winColor =Color.MAGENTA;
														((BoardAdapter)boardGridView.getAdapter()).board[winningLocs[i].getX()+7+(winningLocs[i].getY()*7)].refreshDrawableState();
															}
													}
												}
												frontGridView.setAdapter(frontGridView.getAdapter());
												frontGridView.refreshDrawableState();
												boardGridView.setAdapter(boardGridView.getAdapter());
												boardGridView.refreshDrawableState();
												notWait= true;
										switchTurn();
									}
								});
						         (((BoardAdapter) frontGridView.getAdapter()).getItem(frontGridViewPosition)).startAnimation(animator);
					} else {
						clearing();
					}
				} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
					clearing();
					coloring(v, event);
				} 
				else if (event.getAction() == MotionEvent.ACTION_DOWN) {
					clearing();
					coloring(v, event);
				}
						boardGridView.setAdapter(((BoardAdapter)boardGridView.getAdapter()));
						boardGridView.refreshDrawableState();
						frontGridView.setAdapter(((BoardAdapter)frontGridView.getAdapter()));
						frontGridView.refreshDrawableState();
				}
				return false;
			}

		});
	}
	
	int play(int position) {
		int y = 0;
		while (y < board.length) {
			if (board[y][position] != 0) {
				y--;
				if (y >= 0) {
					board[y][position] = playerTurnColor;
					position = ((y + 1) * 7) + position;
					bounceTill =y+1;
					return position < 49 ? position : -1;
				}
				return -1;
			} else if (y == board.length - 1) {
				board[y][position] = playerTurnColor;
				position = ((y + 1) * 7) + position;
				bounceTill =y+1;
				return position < 49 ? position : -1;
			}
			y++;
		}
		return -1;
	}
	
	private void switchTurn() {
		if (playerTurnColor == homePlayerColor) {
			playerTurnColor = agentPlayerColor;
		} else if (playerTurnColor == agentPlayerColor) {
			playerTurnColor = homePlayerColor;
		}
	}
	
	private boolean isWinner(int y, int x) {
		y = ((y - x) / 7) - 1;
		Point[] locs = new Point[7];
		locs[3] = new Point(x, y);// Diagonal /
		int i = 2;
		int tempX = x;
		int tempY = y;
		int tempX1 = x;
		int tempY1 = y;
		while (i > -1) {
			tempX--;
			tempY++;
			tempX1++;
			tempY1--;
			if (tempX > -1 && tempY < 6) {
				locs[i] = new Point(tempX, tempY);
			}
			if (tempX1 < 7 && tempY1 > -1) {
				locs[6 - i] = new Point(tempX1, tempY1);
			}
			i--;
		}
		int sum = 0;
		winningLocs= new Point[4];
		for (i = 0; i < locs.length; i++) {
			if (locs[i] != null) {
				if (board[locs[i].getY()][locs[i].getX()] == playerTurnColor) {
					winningLocs[sum] = new Point(locs[i].getX(), locs[i].getY());
					sum++;
					if (sum == 4) {
						return true;
					}
				} else {
					sum = 0;
					winningLocs = new Point[4];
				}
			} else {
				sum = 0;
				winningLocs = new Point[4];
			}
		}
		locs = new Point[7];
		locs[3] = new Point(x, y);// Diagonal \
		i = 2;
		tempX = x;
		tempY = y;
		tempX1 = x;
		tempY1 = y;
		while (i > -1) {
			tempX--;
			tempY--;
			tempX1++;
			tempY1++;
			if (tempX > -1 && tempY > -1) {
				locs[i] = new Point(tempX, tempY);
			}
			if (tempX1 < 7 && tempY1 < 6) {
				locs[6 - i] = new Point(tempX1, tempY1);
			}
			i--;
		}
		sum = 0;
		winningLocs = new Point[4];
		for (i = 0; i < locs.length; i++) {
			if (locs[i] != null) {
				if (board[locs[i].getY()][locs[i].getX()] == playerTurnColor) {
					winningLocs[sum] = new Point(locs[i].getX(), locs[i].getY());
					sum++;
					if (sum == 4) {
						return true;
					}
				} else {
					sum = 0;
					winningLocs = new Point[4];
				}
			} else {
				sum = 0;
				winningLocs = new Point[4];
			}
		}
		locs = new Point[7];
		locs[3] = new Point(x, y);// Diagonal -
		i = 2;
		tempX = x;
		tempX1 = x;
		while (i > -1) {
			tempX--;
			tempX1++;
			if (tempX > -1) {
				locs[i] = new Point(tempX, y);
			}
			if (tempX1 < 7) {
				locs[6 - i] = new Point(tempX1, y);
			}
			i--;
		}
		sum = 0;
		winningLocs = new Point[4];
		for (i = 0; i < locs.length; i++) {
			if (locs[i] != null) {
				if (board[locs[i].getY()][locs[i].getX()] == playerTurnColor) {
					winningLocs[sum] = new Point(locs[i].getX(), locs[i].getY());
					sum++;
					if (sum == 4) {
						return true;
					}
				} else {
					sum = 0;
					winningLocs = new Point[4];
				}
			} else {
				sum = 0;
				winningLocs = new Point[4];
			}
		}
		locs = new Point[7];
		locs[3] = new Point(x, y);// Diagonal |
		i = 2;
		tempY = y;
		tempY1 = y;
		while (i > -1) {
			tempY--;
			tempY1++;
			if (tempY > -1) {
				locs[i] = new Point(x, tempY);
			}
			if (tempY1 < 6) {
				locs[6 - i] = new Point(x, tempY1);
			}
			i--;
		}
		sum = 0;
		winningLocs = new Point[4];
		for (i = 0; i < locs.length; i++) {
			if (locs[i] != null) {
				if (board[locs[i].getY()][locs[i].getX()] == playerTurnColor) {
					winningLocs[sum] = new Point(locs[i].getX(), locs[i].getY());
					sum++;
					if (sum == 4) {
						return true;
					}
				} else {
					sum = 0;
					winningLocs = new Point[4];
				}
			} else {
				sum = 0;
				winningLocs = new Point[4];
			}
		}
		winningLocs =null;
		return false;
	}
}

class Point {
	private int x;
	private int y;

	Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}

class BoardAdapter extends BaseAdapter {
	CustomDrawableView[] board;
	private Context context;
	private boolean able;
	public boolean isAble() {
		return able;
	}

	public void setAble(boolean able) {
		this.able = able;
	}

	private int type;
	
	public BoardAdapter(Context context,int type) {
		board = new CustomDrawableView[7*7];
		this.able = true;
		this.context = context;
		this.type = type;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return board.length;
	}

	@Override
	public CustomDrawableView getItem(int position) {
		// TODO Auto-generated method stub
		return board[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return board[position]==null?0:board[position].getId();
	}

	@Override
	public View getView(int position, View item, ViewGroup items) {
		// TODO Auto-generated method stub
		if(item != null) {
			board[position] = (CustomDrawableView) item;
		} else {
				if(able) {
					if(type==1) {
					if (position < 7) {
						board[position] = new CustomDrawableView(context, Color.TRANSPARENT,
								Color.TRANSPARENT);
						board[position].setBackgroundColor(Color.TRANSPARENT);
					} else {
						board[position] = new CustomDrawableView(context, Color.WHITE,
								Color.BLUE);
						board[position].setBackgroundColor(Color.BLUE);
					}
					} else {
						board[position] = new CustomDrawableView(context, Color.TRANSPARENT,
								Color.TRANSPARENT);
						board[position].setBackgroundColor(Color.TRANSPARENT);
					}
				}
					board[position].setLayoutParams(new AbsListView.LayoutParams(
							trial.iconWidth, trial.iconWidth));
					board[position].setPadding(0, 0, 0, 0);
		}
		return board[position];
	}

}

class CustomDrawableView extends View {
	ShapeDrawable smallCircle;
	ShapeDrawable bigCircle;
	int color;
	int winColor;

	public CustomDrawableView(Context context, int custom,int winColor) {
		super(context);
		this.color = custom;
		this.winColor = winColor;
		smallCircle = new ShapeDrawable(new OvalShape());
		bigCircle = new ShapeDrawable(new OvalShape());
	}

	protected void onDraw(Canvas canvas) {
		smallCircle.getPaint().setColor(color);
		bigCircle.getPaint().setColor(winColor);
		setFocusable(false);
		int r = (int) ((trial.iconWidth / 2) - (trial.iconWidth * 0.05));
		int x = (trial.iconWidth) / 2;// position on x axis where the drawing
										// will start
		int y = (trial.iconWidth) / 2;// position on y axis where the drawing
										// will start
		canvas.drawCircle(x, y, r, bigCircle.getPaint());
		canvas.drawCircle(x, y, (int) (r * 0.9), smallCircle.getPaint());
	}

}

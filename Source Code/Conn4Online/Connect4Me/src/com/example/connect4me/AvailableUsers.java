package com.example.connect4me;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;




/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class AvailableUsers extends Activity implements OnClickListener{
	private String selected;
	private Button select;
	private Button refresh;
	private TextView warning;
	
	@Override
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_available_users);
	    
	    select = (Button) findViewById(R.id.Select);
	    select.setOnClickListener(this);
	    
	    refresh = (Button) findViewById(R.id.refresh);
	    refresh.setOnClickListener(this);
	    
	    warning = (TextView) findViewById(R.id.warning1);
	    warning.setText("Pick User to Chat");
	    
	    final ListView listview = (ListView) findViewById(R.id.listView1);
	    Intent j = getIntent();
		Bundle b = j.getExtras();
		String clientNames = b.getString("users");
		String [] values = clientNames.split("-");
	    final ArrayList<String> list = new ArrayList<String>();
	    for (int i = 0; i < values.length; ++i) {
	      list.add(values[i]);
	    }
	    final StableArrayAdapter adapter = new StableArrayAdapter(this,
	        android.R.layout.simple_list_item_1, list);
	    listview.setAdapter(adapter);

	    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	      @Override
	      public void onItemClick(AdapterView<?> parent, final View view,
	          int position, long id) {
	        selected = (String) parent.getItemAtPosition(position);
	      }

	    });
	  }
	
	

	  private class StableArrayAdapter extends ArrayAdapter<String> {

	    HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

	    public StableArrayAdapter(Context context, int textViewResourceId,
	        List<String> objects) {
	      super(context, textViewResourceId, objects);
	      for (int i = 0; i < objects.size(); ++i) {
	        mIdMap.put(objects.get(i), i);
	      }
	    }

	    @Override
	    public long getItemId(int position) {
	      String item = getItem(position);
	      return mIdMap.get(item);
	    }

	    @Override
	    public boolean hasStableIds() {
	      return true;
	    }

	  }



	@Override
	public void onClick(View v) {
		if(v == select) {
			if(selected != null) {
				this.refresh.setVisibility(View.INVISIBLE);
				this.select.setVisibility(View.INVISIBLE);
				this.warning.setText("Trying to connect..");
				Variables.outToServer.println("3"+selected);
				try {
					String inMsg = Variables.inFromServer.readLine();
					boolean t= true;
					while (t) {
						if(inMsg != null) {
							if(inMsg.startsWith("3")) {
								inMsg = inMsg.substring(1);
							if (inMsg.equalsIgnoreCase("True")){
								Intent i = new Intent(this,ChatPage.class);
								Bundle b = new Bundle();
								i.putExtras(b);
								startActivity(i);
								t= false;
							} else if(inMsg.equalsIgnoreCase("Flase")) {
								this.warning.setText("Please pick another one");
								this.refresh.setVisibility(View.VISIBLE);
								this.select.setVisibility(View.VISIBLE);
								t= false;
							}
							} else if (inMsg.startsWith("6")) {
								Intent i = new Intent(this,ChatPage.class);
								Bundle b = new Bundle();
								i.putExtras(b);
								startActivity(i);
								t= false;
							}
						}
						inMsg = Variables.inFromServer.readLine();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else if(v== refresh){
			Variables.outToServer.println("2");
			String users;
			boolean t = true;
			while(t) {
					try {
						users = Variables.inFromServer.readLine();
						if(users != null) {
							if(users.startsWith("2")) {
								users = users.substring(1);
							Intent i = new Intent(this,AvailableUsers.class);
							Bundle b = new Bundle();
							b.putString("users", users);
							i.putExtras(b);
							startActivity(i);
							t = false;
							return;
					}  else if (users.startsWith("6")) {
						Intent i = new Intent(this,ChatPage.class);
						Bundle b = new Bundle();
						i.putExtras(b);
						startActivity(i);
						t = false;
						return;
					}
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
					
			}
		}
	}
}

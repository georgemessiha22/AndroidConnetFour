package com.example.connect4me;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class ChatPage extends Activity implements OnClickListener,Runnable{
	
	private Button send;
	private TextView messages;
	private EditText text;
	private String messagess;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat_page);
		
		send = (Button) findViewById(R.id.send);
		send.setOnClickListener(this);
		
		messages = (TextView) findViewById(R.id.messages);
		messages.setText("");
		
		text = (EditText) findViewById(R.id.textmessage);
		text.setText("");
		
		messagess ="";

		messages.refreshDrawableState();
		text.refreshDrawableState();
		send.refreshDrawableState();

	}


	@Override
	public void onClick(View v) {
		if( v ==send ) {
			Variables.outToServer.println("4"+text.getText().toString());
			messagess=messagess+ "\n you:"+ text.getText().toString();
			messages.setText(messagess);
			text.setText("");
			messages.refreshDrawableState();
			text.refreshDrawableState();
		}
		String mes;
		boolean t= true;
			try {
				while(t){
				mes = Variables.inFromServer.readLine();
				if(mes != null ) {
					if(mes.startsWith("5")){
						messagess = messagess + "\n"+ mes.substring(1);
					messages.setText(messagess);
					messages.refreshDrawableState();
					text.refreshDrawableState();
					t=false;
					return;
					} else if(mes.startsWith("7")){
						Variables.outToServer.println("2");
				    	boolean f= true;
							try {
								while(f) {
								String users = Variables.inFromServer.readLine();
								if(users != null) {
									if(users.startsWith("2")) {
										users = users.substring(1);
									Intent i = new Intent(this,AvailableUsers.class);
									Bundle b = new Bundle();
									b.putString("users", users);
									i.putExtras(b);
									startActivity(i);
									f=false;
									return;
							}  else if (users.startsWith("6")) {
								Intent i = new Intent(this,ChatPage.class);
								Bundle b = new Bundle();
								i.putExtras(b);
								startActivity(i);
								f=false;
								return;
							}
								}
								}
							} catch (IOException e) {
								e.printStackTrace();
							}
					}
				}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	    	Variables.outToServer.println("7");
	    	Variables.outToServer.println("2");
	    	boolean t= true;
			while(t) {
				try {
					String users = Variables.inFromServer.readLine();
					if(users != null) {
						if(users.startsWith("2")) {
							users = users.substring(1);
						Intent i = new Intent(this,AvailableUsers.class);
						Bundle b = new Bundle();
						b.putString("users", users);
						i.putExtras(b);
						startActivity(i);
						t=false;
				}  else if (users.startsWith("6")) {
					Intent i = new Intent(this,ChatPage.class);
					Bundle b = new Bundle();
					i.putExtras(b);
					startActivity(i);
					t=false;
				}
						}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	    }
	    return super.onKeyDown(keyCode, event);
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
}

package com.example.connect4me;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class Variables {
	
	/*
	 * The client's unique ID.
	 */
	protected static int clientID;
	/*
	 * The client-side socket.
	 */
	protected static Socket clientSocket;
	/*
	 * A BufferedReader instance that enables the client to read incoming
	 * messages that are sent by the server.
	 */
	protected static BufferedReader inFromServer;
	/*
	 * A PrintWriter instance that enables the client to send out messages to
	 * the server.
	 */
	protected static PrintWriter outToServer;
	/*
	 * Threading is required to avoid the NetworkOnMainThreadException. Explain
	 * to them the concept of main UI and background threading, and why it is
	 * essential that all networking operations be performed using threading.
	 * Also, STRESS on the fact that networking operations CANNOT be performed
	 * in the onCreate method without the use of a thread.
	 */
	protected static Thread thread;
	

}

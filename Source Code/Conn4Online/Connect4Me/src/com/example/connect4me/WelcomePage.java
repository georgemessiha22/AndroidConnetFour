package com.example.connect4me;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class WelcomePage extends Activity implements OnClickListener,Runnable{
	
	private Button start;
	private Button exit;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_welcome_page);

		start = (Button) findViewById(R.id.Start);
		exit = (Button) findViewById(R.id.Exit);
		
		start.setOnClickListener(this);
		exit.setOnClickListener(this);
		
		/*
		 * A new thread instance is created and started. Starting the thread
		 * instance causes the run() method of this thread to start executing.
		 */
		Variables.thread = new Thread(this);
		Variables.thread.start();
	}

	@Override
	public void onClick(View view) {
		if(view == start){
			Intent i = new Intent(this, Login.class);
			Bundle b = new Bundle();
			i.putExtras(b);
			startActivity(i);
		} else if(view == exit){
			this.finish();
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			Variables.clientSocket = new Socket("41.130.239.217", 5151);
			Variables.inFromServer = new BufferedReader(new InputStreamReader(
					Variables.clientSocket.getInputStream()));
			Variables.outToServer = new PrintWriter(Variables.clientSocket.getOutputStream(), true);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}

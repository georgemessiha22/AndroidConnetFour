package com.example.connect4me;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Login extends Activity  implements OnClickListener{
	private Button next;
	private EditText text;
	private ProgressBar progressing;
	private TextView warning;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		 next = (Button) findViewById(R.id.Next);
		 next.setOnClickListener(this);
		 
		 text = (EditText)findViewById(R.id.username);
         text.setOnClickListener(this);
         
         progressing = (ProgressBar) findViewById(R.id.progressBar1);
         progressing.setVisibility(View.INVISIBLE);
         
         warning = (TextView) findViewById(R.id.warninglogin);
		 
	}

	@Override
	public void onClick(View view) {
		if (view == next) {
			 String username = text.getText().toString();
			 this.onPause();
			if(username.length() != 0){
			Variables.outToServer.println("1"+username);
			boolean t= true;
			String response;
				try {
					while(t){
					response = Variables.inFromServer.readLine();
					System.out.println(response);
					if(response != null){
						if(response.startsWith("1")) {
							response = response.substring(1);
						if(response.equalsIgnoreCase("True")){
							Variables.outToServer.println("2");
							boolean f= true;
							while(f) {
								response = Variables.inFromServer.readLine();
								if(response.startsWith("2")) {
									response = response.substring(1);
									if(response != null) {
										Intent i = new Intent(this,AvailableUsers.class);
										Bundle b = new Bundle();
										b.putString("users", response);
										i.putExtras(b);
										startActivity(i);
										f= false;
										t= false;
								}
								} else if (response.startsWith("6")) {
									response = response.substring(1);
									Intent i = new Intent(this,ChatPage.class);
									Bundle b = new Bundle();
									i.putExtras(b);
									startActivity(i);
									f= false;
									t= false;
								}
							}
						} else if(response.equalsIgnoreCase("False")) {
							this.onResume();
							warning.setText("try another username :)");
							t= false;
						}
					}
					}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void onPause() {
	    super.onPause();  // Always call the superclass method first

	    // Release the Camera because we don't need it when paused
	    // and other activities might need to use it.
		text.setText("");
		text.setVisibility(View.INVISIBLE);
		next.setVisibility(View.INVISIBLE);
		warning.setVisibility(View.INVISIBLE);
		progressing.setVisibility(View.VISIBLE);
	}
	
	public void onResume(){
		super.onResume();
		text.setVisibility(View.VISIBLE);
		next.setVisibility(View.VISIBLE);
		progressing.setVisibility(View.INVISIBLE);
		return;
	}

	
}

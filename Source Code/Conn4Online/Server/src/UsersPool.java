import java.util.ArrayList;

public class UsersPool {
	ArrayList<ClientThread> users;
	ArrayList<Channel> channels;

	public UsersPool() {
		users = new ArrayList<ClientThread>();
		channels = new ArrayList<Channel>();
	}

	protected void addClient(ClientThread c) {
		this.users.add(c);
	}

	public void removeClient(ClientThread clientThread) {
		if (clientThread != null) {
			for (int i = 0; i < users.size(); i++) {
				if(users.get(i) == null) {
					users.remove(i);
				}else if (users.get(i).equals(clientThread)) {
					users.remove(i);
				}
			}
		}
	}

	public String initChannel(ClientThread user1, String user2) {
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i) != null && users.get(i).getUsername() != null) {
				if (users.get(i).getUsername().equalsIgnoreCase(user2)) {
					if (!users.get(i).isBusy()) {
						channels.add(new Channel(user1, users.get(i)));
						user1.setBusy(true);
						users.get(i).setBusy(true);
						users.get(i).startChat(user1.getUsername());
						System.out.println(user1.getUsername()
								+ " trying to call " + users.get(i).getUsername());
						return "true" + users.get(i).getUsername();
					} else {
						return "false"+"";
					}
				}
			} else if (users.get(i) == null) {
				users.remove(i);
			}
		}

		return "false"+"";
	}

	public void breakChannel(ClientThread user1, boolean b) {
		for (int each = 0; each < channels.size(); each++) {
			if (channels.get(each).getUser1().getUsername()
					.equalsIgnoreCase(user1.getUsername())
					|| channels.get(each).getUser2().getUsername()
							.equalsIgnoreCase(user1.getUsername())) {
				channels.get(each).getUser1().setBusy(false);
				channels.get(each).getUser2().setBusy(false);
				if(b) {
				channels.get(each).getUser1().outToClient.println("7");
				channels.get(each).getUser2().outToClient.println("7");
				}
				channels.remove(each);
			}
		}
	}

	public boolean sendMessage(ClientThread c, String msg) {
		for (Channel each : channels) {
			if (c.getUsername().equalsIgnoreCase(each.getUser1().getUsername())) {
				each.getUser2().rcvMsg(msg);
				return true;
			} else if (c.getUsername().equalsIgnoreCase(
					each.getUser2().getUsername())) {
				each.getUser1().rcvMsg(msg);
				return true;
			}
		}
		return false;
	}

	public boolean usernameAvailable(String name) {
		for (ClientThread each : users) {
			if (each.getUsername() != null) {
				if (each.getUsername().equalsIgnoreCase(name)) {
					return false;
				}
			}
		}
		return true;
	}

	public String avilableUsers(ClientThread c) {
		String us = "";
		for (ClientThread each : this.users) {
			if (!each.isBusy()) {
				if (each.getUsername() != null) {
					if (!each.getUsername().equalsIgnoreCase(c.getUsername())) {
						us = us + each.getUsername() + "-";
					}
				}
			}
		}
		return us;
	}

	public void inFormOthers(String name) {
		String us;
		for (ClientThread each : this.users) {
			if (each != null) {
				if (!each.isBusy()) {
					if (each.getUsername() != null) {
						if (!each.getUsername().equalsIgnoreCase(name)) {
							us = "";
							for (ClientThread each1 : this.users) {
								if (each1 != null) {
									if (!each1.isBusy()) {
										if (each1.getUsername() != null) {
											if (!each1.getUsername()
													.equalsIgnoreCase(
															each.getUsername())) {
												us = us + each1.getUsername()
														+ "-";
											}
										}
									}
								}
							}
							each.outToClient.println("9" + us);
						}
					}
				}
			}
		}
	}
}

class Channel {
ClientThread user1;
ClientThread user2;
public Channel(ClientThread user1, ClientThread user2) {
	this.user1 = user1;
	this.user2 = user2;
}
public ClientThread getUser1() {
	return user1;
}
public void setUser1(ClientThread user1) {
	this.user1 = user1;
}
public ClientThread getUser2() {
	return user2;
}
public void setUser2(ClientThread user2) {
	this.user2 = user2;
}

}


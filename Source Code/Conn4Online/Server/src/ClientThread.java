import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/*
 * This class represents the interaction between the server and each individual client connection. 
 * As such, this class should extend the Thread class. Extending the Thread class requires a run() 
 * method to be present within this class.
 */
public class ClientThread extends Thread {
	/*
	 * The client's unique ID.
	 */
	protected String clientID;
	/*
	 * The client-side socket.
	 */
	protected Socket clientSocket;
	/*
	 * A BufferedReader instance that enables the server to read incoming
	 * messages that are sent by the client.
	 */
	protected BufferedReader inFromClient;
	/*
	 * A PrintWriter instance that enables the server to send out messages to
	 * the client.
	 */
	protected PrintWriter outToClient;
	protected boolean busy;
	protected UsersPool u;
	protected int score;

	public ClientThread(Socket clientSocket, UsersPool clients) {
		try {
			this.clientSocket = clientSocket;
			this.u = clients;
			inFromClient = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));
			outToClient = new PrintWriter(clientSocket.getOutputStream(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean isBusy() {
		return busy;
	}

	public void setBusy(boolean b) {
		busy = b;
		if(!b) {
			u.inFormOthers(getUsername());
		}
	}

	public void run() {
		/*
		 * Notice that the following code should be placed in a while(true) loop
		 * to enable the server to be able to read any messages that are coming
		 * from the client at ANY time. In other words, the server cannot
		 * predict when a message is going to be sent in from the client.
		 */
		while (true) {
			try {
				/*
				 * This command reads the incoming client message from the
				 * BufferedReader instance.
				 */
				String inMsg = inFromClient.readLine();
				/*
				 * If there is, indeed, an incoming client message...
				 */
				if (inMsg != null) {
					if (inMsg.startsWith("1")) {
						String name = inMsg.substring(1);
						if (this.u.usernameAvailable(name)) {
							this.outToClient.println("1" + "True");
							this.clientID = name;
							this.u.inFormOthers(name);
						} else {
							this.outToClient.println("1" + "False");
						}
					} else if (inMsg.startsWith("2")) {
						this.outToClient.println("2"
								+ this.u.avilableUsers(this));
					} else if (inMsg.startsWith("3")) {
						this.outToClient.println("3"
								+ this.u.initChannel(this, inMsg.substring(1)));
					} else if (inMsg.startsWith("4")) {
						this.u.sendMessage(this, inMsg.substring(1));
					} else if (inMsg.startsWith("7")) {
						this.u.breakChannel(this,true);
					} else if(inMsg.startsWith("9")) {
						this.u.breakChannel(this,true);
						this.u.removeClient(this);
					} else if(inMsg.startsWith("S")) {
						outToClient.println("S"+score);
					} else if (inMsg.startsWith("W")) {
						this.u.breakChannel(this,false);
						this.score++;
					}
				}
			} catch (IOException e) {
				this.u.removeClient(this);
				this.outToClient.close();
				try {
					this.inFromClient.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	public void rcvMsg(String msg) {
		this.outToClient.println("5" + msg);
	}

	public void startChat(String username) {
		this.outToClient.println("6"+username);
	}

	public String getUsername() {
		return this.clientID;
	}
}

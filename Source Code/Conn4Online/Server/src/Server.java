import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	/*
	 * The unique ID that will be given to the next client that connects to the
	 * server.
	 */
	protected int nextClientID;
	/*
	 * The server-side socket that will keep listening for incoming client
	 * connections.
	 */
	protected ServerSocket serverSocket;
	/*
	 * A LinkedList of ClientThread instances, where each ClientThread is
	 * responsible for dealing with a particular client.
	 */
	protected UsersPool clients;

	public Server(int port) {
		try {
			nextClientID = 1;
			serverSocket = new ServerSocket(port);
			clients = new UsersPool();
			System.out.println("Server is now running on port " + port);
			while (true) {
				Socket clientSocket = serverSocket.accept();
				ClientThread clientThread = new ClientThread(clientSocket,
						clients);
				clients.addClient(clientThread);
				System.out.println("Server welcome" + nextClientID);
				clientThread.start();
				nextClientID++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new Server(5151);
	}
}
